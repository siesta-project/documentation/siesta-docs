TBTrans User Guide
******************
.. container:: titlepage

   .. container:: center

      D e v e l o p e r’ s     G u i d e

      **T B t r a n s    **

      https://gitlab.com/siesta-project/siesta

Contributors to TBtrans
=======================

TBtrans is Copyright © 2016-2021 by Nick R. Papior. The original
TBtrans code was implemented by Mads Brandbyge, Jose L. Mozos, Jeremy
Taylor, Pablo Ordejon and Kurt Stokbro. The current TBtrans is
implemented by the following contributors:

================== =================================
Nick Rübner Papior *Technical University of Denmark*
\                  
================== =================================

Introduction
============

*This Reference Manual contains descriptions of all the input, output
and execution features of TBtrans, but is not a tutorial introduction to
the program.*

TBtrans (Tight-Binding transport) is a generic computer program which
calculates transport and other physical quantities using the Green
function formalism. It is a stand-alone program which allows *extreme*
scale tight-binding calculations.

- It uses the basic non-equilibrium Green function formalism and allows
  extensive customizability and analysis forms.

- TBtrans may be given any type of local-orbital Hamiltonian and
  calculate transport properties of arbitrary geometries and/or number
  of electrodes.

- The PHtrans variant may be compiled to obtain thermal (phonon)
  transport using the same Green function formalism and *all* the same
  functionalities as those presented in this manual.

As TBtrans output has changed to the flexible NetCDF-4 format you are
highly encouraged to use the sisl\ (N. R. Papior 2020) toolsuite which
has nearly all the necessary tools available to perform advanced
analysis. If used, please cite sisl appropriately.

A list of the currently implemented features are:

- Density of states (orbital resolved)

  - Green function DOS

  - Scattering DOS

- Hamiltonian interpolation at different voltages

- Selective wide-band limit of the electrode(s)

- Transmission eigenvalues

- Bulk electrode density of state and transmission (directly from the
  electrode Hamiltonian)

- Projected transmission of eigenstates

- Orbital resolved “bond-currents” which may subsequently be analyzed to
  yield actual bond-currents

- Density matrices using the Green function and/or the spectral density

- COOP and COHP curves using the Green function and/or the spectral
  density.

**References:**

- Description of the TBtrans and TranSIESTA code in the :math:`N`
  terminal generic implementation (N. Papior et al. 2017).

- sisl is a data analysis/extraction utility for TBtrans which enables
  easy access to the data stored in the output NetCDF-4 file (N. R.
  Papior 2020).

PHtrans
-------

The NEGF formalism also applies to phonons via some simple differences.
Here is a list of some of the differences:

- For PHtrans all options are *still* prefixed with TBT!

- The Green function calculation looks like:

  .. math::



         \textbf{G}_{\textbf{q}} = [(\omega^2 +i\eta^2)\textbf{I} -\textbf{D}_{\textbf{q}} -\boldsymbol\Sigma_{\textbf{q}}(\omega)]^{-1},

  where :math:`\omega` is referred to as *energy* in the remaining
  document.

- Calculating density matrices
  (:ref:`TBT.DM.Gf<fdfparam:tbt.dm.gf>`,
  :ref:`TBT.COOP.Gf<fdfparam:tbt.coop.gf>`,
  :ref:`TBT.COHP.Gf<fdfparam:tbt.cohp.gf>`)
  are prefactored with :math:`2\omega` which is currently empirically
  done.

**NOTE:** PHtrans is not as tested as TBtrans. Any feedback on all parts
are most welcome!

Compilation
===========

TBtrans will be compiled when together with SIESTA when issuing
``cmake``.

If only the TBtrans/PHtrans executables are desired, say for
tight-binding calculations, one should execute the ``cmake`` build like
this:

::

     cmake -B _build --target SIESTA.tbtrans SIESTA.phtrans

Execution of the Program
========================

TBtrans should be called with an input file which defines what *it
should do*. This may either be piped or simply added on the input line.
The latter method is preferred as one may use flags for the executable.

::

     $ tbtrans < RUN.fdf
     $ tbtrans RUN.fdf

Note that if TBtrans is compiled with MPI support one may call it like

::

     $ mpirun -np 4 tbtrans RUN.fdf

for :math:`4` MPI-processors.

TBtrans has these optional flags:

-help or -h
   print a help instruction and quit

-version or -v
   print TBtrans version and quit

-out or -o
   specify where all output should be written to (instead of STDOUT)

-L
   override
   :ref:`SystemLabel<fdfparam:systemlabel>`
   flag

-V
   override
   :ref:`TBT.Voltage<fdfparam:tbt.voltage>`
   flag. To denote the unit do as this example: -V 0.2:eV which sets the
   voltage to :math:`0.2\,\mathrm{eV}`. A value without unit is
   interpreted as :math:`\mathrm{eV}`.

-D
   override
   :ref:`TBT.Directory<fdfparam:tbt.directory>`
   flag, all output of TBtrans will be put in the corresponding folder
   (it will be created if non-existing)

-HS
   specify the
   :ref:`TBT.HS<fdfparam:tbt.hs>`
   variable, quickly override the used Hamiltonian

-fdf
   specify any given fdf flag on the command line, example -fdf
   TBT.Voltage:0.2:eV

Note that for all flags one may use “:” as a replacement for “ ”,
although one may use quotation marks when having a space in the
argument.

fdf-flags
=========

Although TBtrans is a fully independent Green function transport code,
it is hard-wired with the TranSIESTA fdf flags and options. If you are
familiar with TranSIESTA and its input flags, then the use of
TBtrans should be easy.

All fdf-flags for TBtrans are defaulted to their equivalent
TranSIESTA flag. Thus if you are using TranSIESTA as a back-end you
should generally not change any flags. For instance
:ref:`TBT.Voltage<fdfparam:tbt.voltage>`
defaults to TS.Voltage if not supplied.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:systemlabel

      fdfparam:SystemLabel

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         SystemLabel

      .. container:: fdfparamdefault

         siesta

   .. container:: fdfentrycontainerbody

      The label defining this calculation. All relevant output will be
      prefixed with the SystemLabel.

      One may start several TBtrans calculations in the same directory
      if they have different labels.

.. container:: fdfentrycontainer fdfentry-energy

   .. container:: labelcontainer
      :name: fdfparam:tbt.voltage

      fdfparam:TBT.Voltage

   .. container:: fdfparamtype

      energy

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Voltage

      .. container:: fdfparamdefault

         :math:`0\,\mathrm{eV}`

   .. container:: fdfentrycontainerbody

      Define the applied bias in the scattering region.

.. container:: fdfentrycontainer fdfentry-directory

   .. container:: labelcontainer
      :name: fdfparam:tbt.directory

      fdfparam:TBT.Directory

   .. container:: fdfparamtype

      directory

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Directory

      .. container:: fdfparamdefault

         ./

   .. container:: fdfentrycontainerbody

      Define the output directory of files from TBtrans. This allow
      execution of several TBtrans instances in the same folder and
      writing their result to different, say, sub-folders. It is
      particularly useful for interpolation of Hamiltonian’s and for
      testing purposes.

.. container:: fdfentrycontainer fdfentry-integer

   .. container:: labelcontainer
      :name: fdfparam:tbt.verbosity

      fdfparam:TBT.Verbosity

   .. container:: fdfparamtype

      integer

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Verbosity

      .. container:: fdfparamdefault

         5

   .. container:: fdfentrycontainerbody

      Specify how much information TBtrans will print-out (range 0-10).

      For smaller numbers, less information will be printed, and for
      larger values, more information is printed.

.. container:: fdfentrycontainer fdfentry-real

   .. container:: labelcontainer
      :name: fdfparam:tbt.progress

      fdfparam:TBT.Progress

   .. container:: fdfparamtype

      real

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Progress

      .. container:: fdfparamdefault

         5.

   .. container:: fdfentrycontainerbody

      TBtrans prints out an estimated time of completion (ETA) for the
      calculation. By default this is printed out every 5% of the total
      loops (:math:`k`-point :math:`\times` energy loops). Setting this
      to 0 will print out after every energy loop.

Define electronic structure
---------------------------

.. container:: fdfentrycontainer fdfentry-file

   .. container:: labelcontainer
      :name: fdfparam:tbt.hs

      fdfparam:TBT.HS

   .. container:: fdfparamtype

      file

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.HS

      .. container:: fdfparamdefault

         <SystemLabel>.TSHS

   .. container:: fdfentrycontainerbody

      Define the Hamiltonian file which contains information regarding
      the Hamiltonian and geometry.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.hs.files

      fdfparam:TBT.HS.Files

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.HS.Files

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      A list of files which each contain the Hamiltonian for the same
      geometry at different bias’. Each line has three entries, 1) the
      :ref:`TBT.HS<fdfparam:tbt.hs>`
      file, 2) the value of the bias applied, 3) the unit of the bias.

      **NOTE:** if this is existing it will assume that you will perform
      an interpolation of the Hamiltonians to the corresponding bias
      (:ref:`TBT.Voltage<fdfparam:tbt.voltage>`).

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.hs.interp

      fdfparam:TBT.HS.Interp

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.HS.Interp

      .. container:: fdfparamdefault

         spline|linear

   .. container:: fdfentrycontainerbody

      Interpolate all files defined in
      :ref:`TBT.HS.Files<fdfparam:tbt.hs.files>`
      to the corresponding applied bias.

      Generally spline produces the best interpolated values and its use
      is encouraged. The linear interpolation scheme is mainly used for
      comparison to the spline. If they are very different from each
      other then one may be required to perform additional
      self-consistent calculations at the specific bias due to large
      changes in the electronic structure.

Say you have calculated the SCF solution of a certain system at 5
different applied bias’:

::

     %block TBT.HS.Files
       ../V0/siesta.TSHS     0.  eV
       ../V-0.5/siesta.TSHS -0.5 eV
       ../V0.5/siesta.TSHS   0.5 eV
       ../V-1.0/siesta.TSHS -1.0 eV
       ../V1.0/siesta.TSHS   1.0 eV
     %endblock

and you wish to calculate the interpolated transmissions and currents at
steps of :math:`0.1\,\mathrm{eV}`, then you may use this simple loop

::

     for V in `seq -1.5 0.1 1.5` ; do
        tbtrans -V $V:eV -D V$V RUN.fdf
     done

which at each execution of TBtrans interpolates the Hamiltonian to the
corresponding applied bias and store all output files in the ``V$V``
folder.

Changing the electronic structure via :math:`\delta` elements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The electronic structure may be altered by changing the Hamiltonian
elements via a simple additive term

.. math:: \textbf{H} \leftarrow \textbf{H} + \delta\textbf{H}+ \delta\textbf{\Sigma},

which allows easy changes to the electronic structure or adding
additional terms such as imaginary self-energies. One may also use it to
add magnetic fields etc.

TBtrans uses a distinction between :math:`\delta\textbf{H}` and
:math:`\delta\textbf{\Sigma}` only via the orbital current calculation.
I.e. :math:`\delta\textbf{H}` enters the equations for calculating the
orbital current, whereas :math:`\delta\textbf{\Sigma}` does not.
Otherwise the two :math:`\delta`-terms are *completely* identical. In
the following discussion we will use the term :math:`\delta` to be
either :math:`\delta\textbf{H}` or :math:`\delta\textbf{\Sigma}`.

To use this feature at :math:`k` points it is important to know that
phases in TBtrans are defined using the lattice vectors (and *not*
inter-atomic distances)

.. math::

   \textbf{H}_k = \textbf{H} \cdot e^{i k \cdot \textbf{R}}.
     
       

TBtrans will add the phases on *all* elements of :math:`\delta` via
Eq. `[eq:hk-phase] <#eq:hk-phase>`__. To counter these phases one may
simply multiply :math:`\delta` with the negative phase (:math:`-i`).
Note that phases are only added on super cell elements, *not* unit cell
elements.

.. container:: fdfentrycontainer fdfentry-file

   .. container:: labelcontainer
      :name: fdfparam:tbt.dh

      fdfparam:TBT.dH

   .. container:: fdfparamtype

      file

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.dH

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      Denote a file which contains the :math:`\delta\textbf{H}`
      information.

      **NOTE:** that the terms defined in this file are added to the
      Hamiltonian when calculating the orbital currents, if your terms
      are not a Hamiltonian change, then consider using
      :ref:`TBT.dSE<fdfparam:tbt.dse>`
      instead.

      This file *must* adhere to these file format notations and is
      required to be supplied in a NetCDF4 format

      ::

         [fontsize=\footnotesize]
         netcdf file.dH {
         dimensions:
             one = 1 ;
             n_s = 9 ;
             xyz = 3 ;
             no_u = 900 ;
             spin = 1 ;
         variables:
             int nsc(xyz) ;
                 nsc:info = "Number of supercells in each unit-cell direction" ;

         group: LEVEL-1 {
           dimensions:
             nnzs = 2670 ;
           variables:
             int n_col(no_u) ;
                 n_col:info = "Number of non-zero elements per row" ;
             int list_col(nnzs) ;
                 list_col:info = "Supercell column indices in the sparse format" ;
             int isc_off(n_s, xyz) ;
                 isc_off:info = "Index of supercell coordinates" ;
             double Redelta(spin, nnzs) ;
                 Redelta:info = "Real part of delta" ;
                 Redelta:unit = "Ry" ;
             double Imdelta(spin, nnzs) ;
                 Imdelta:info = "Imaginary part of delta" ;
                 Imdelta:unit = "Ry" ;
           } // group LEVEL-1

         group: LEVEL-2 {
           dimensions:
             nkpt = UNLIMITED ;
             nnzs = 2670 ;
           variables:
             double kpt(nkpt, xyz) ;
                 kpt:info = "k-points for delta values" ;
                 kpt:unit = "b**-1" ;
             ... n_col list_col isc_off ...
             double delta(nkpt, spin, nnzs) ;
                 delta:info = "delta" ;
                 delta:unit = "Ry" ;
           } // group LEVEL-2

         group: LEVEL-3 {
           dimensions:
             ne = UNLIMITED ;
             nnzs = 2670 ;
           variables:
             double E(ne) ;
                 E:info = "Energy points for delta values" ;
                 E:unit = "Ry" ;
             ... n_col list_col isc_off ...
             double delta(ne, spin, nnzs) ;
                 delta:info = "delta" ;
                 delta:unit = "Ry" ;
           } // group LEVEL-3

         group: LEVEL-4 {
           dimensions:
             nkpt = UNLIMITED ;
             ne = UNLIMITED ;
             nnzs = 2670 ;
           variables:
             double kpt(nkpt, xyz) ;
                 kpt:info = "k-points for delta values" ;
                 kpt:unit = "b**-1" ;
             double E(ne) ;
                 E:info = "Energy points for delta values" ;
                 E:unit = "Ry" ;
             ... n_col list_col isc_off ...
             double delta(nkpt, ne, spin, nnzs) ;
                 delta:info = "delta" ;
                 delta:unit = "Ry" ;
           } // group LEVEL-4
         }

      This example file shows how the file should be formatted. Note
      that one may either define the Hamiltonian as ``delta`` or as
      ``Redelta`` and ``Imdelta``. The former is defining :math:`\delta`
      as a real quantity while the latter makes it an imaginary
      :math:`\delta`.

      The levels are defined because they have precedence from each
      other, if the energy point and :math:`k` point is found in LEVEL-4
      it will use this, if not, it will check for the energy point in
      LEVEL-3, and so on.

The remaining options are only applicable if
:ref:`TBT.dH<fdfparam:tbt.dh>`
has been set.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.dh.parallel

      fdfparam:TBT.dH!Parallel

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.dH.Parallel

      .. container:: fdfparamdefault

         true

   .. container:: fdfentrycontainerbody

      Whether the :math:`\delta\textbf{H}` file should be read in
      parallel. If your architecture supports parallel IO it is
      beneficial to do so. TBtrans performs a basic check whether
      parallel IO may be possible, if it cannot assert this it will be
      turned off.

.. container:: fdfentrycontainer fdfentry-file

   .. container:: labelcontainer
      :name: fdfparam:tbt.dse

      fdfparam:TBT.dSE

   .. container:: fdfparamtype

      file

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.dSE

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      File has same format as specified for
      :ref:`TBT.dH<fdfparam:tbt.dh>`.

      The only difference between a :math:`\delta\textbf{H}` and
      :math:`\delta\textbf{\Sigma}` file is that the terms in
      :math:`\delta\textbf{\Sigma}` does *not* enter the calculation of
      the bond-currents, whereas :math:`\delta\textbf{H}` does, see
      Eq. `[eq:bond-current] <#eq:bond-current>`__.

Determine calculated physical quantities
----------------------------------------

.. container:: labelcontainer
   :name: sec:physical

   sec:physical

TBtrans can calculate a large variety of physical quantities. By default
it will only calculate the transmission between the electrodes.
Calculating as few quantities as possible will increase throughput,
while requesting many quantities will result in much longer run-times.

You are heavily encouraged to compile TBtrans with NetCDF4 support, see
Sec. :ref:`arch-make<sec:arch-make>`,
as quantities will be orbital resolved.

If TBtrans has been compiled with NetCDF4 support, one may extract the
projected DOS from the TBT.nc using sisl (or manual scripting). The
calculated DOS can *only* be extracted from the atoms in the device
region (atoms in block
:ref:`TBT.Atoms.Device<fdfparam:tbt.atoms.device>`).
Hence the
:ref:`TBT.Atoms.Device<fdfparam:tbt.atoms.device>`
block is *extremely* important when conducting detailed DOS analysis.
For instance if the input file has this:

::

     %block TBT.Atoms.Device 
       atom [20 -- 40]
     %endblock

one may extract the PDOS on a subset of atoms using this sisl command

::

     sdata siesta.TBT.nc --atom 20-30 --dos --ados Left --out dos_20-30.dat
     sdata siesta.TBT.nc --atom 20-30[1-3] --dos --ados Left --out dos_20-30_1-3.dat

where the former is the total PDOS on atoms :math:`20` through
:math:`30`, and the latter is the PDOS on orbitals 1, 2 and 3 on atoms
:math:`20` through :math:`30`. It thus is extremely easy to extract
different PDOS once the calculation has completed.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.t.bulk

      fdfparam:TBT.T!Bulk

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.T.Bulk

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the bulk (pristine) electrode transmission if **true**.

      This generates BTRANS\_<> and AVBTRANS\_<>.

      **NOTE:** implicitly enables
      :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`
      if **true**.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.dos.elecs

      fdfparam:TBT.DOS!Elecs

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.DOS.Elecs

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the bulk (pristine) electrode DOS if **true**.

      This generates BDOS\_<> and AVBDOS\_<>.

      **NOTE:** implicitly enables
      :ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`
      if **true**.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.dos.gf

      fdfparam:TBT.DOS!Gf

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.DOS.Gf

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the DOS from the Green function on the atoms in the
      device region:

      .. math::

         \begin{aligned}
             
             

             \textbf{G}(E) &= [E\textbf{S} - \textbf{H} -
             \sum_{\mathfrak e}\boldsymbol\Sigma_{\mathfrak e}(E)]^{-1}
             \\
             &=
             \sum_{\mathfrak e}\textbf{G}(E)\boldsymbol\Gamma_{\mathfrak
                 e}(E)\textbf{G}^\dagger(E) + \text{bound states}
             \\
             &=
             \sum_{\mathfrak e}\textbf{A}_{\mathfrak e}(E) + \text{bound states}
           
         \end{aligned}

      **NOTE:** this flag should only be used if there are bound states
      in the scattering region (or if one wish to uncover whether there
      are bound states). Due to internal algorithms the DOS from the
      Green function is computationally more demanding than using
      :ref:`TBT.DOS.A<fdfparam:tbt.dos.a>`
      and
      :ref:`TBT.DOS.A.All<fdfparam:tbt.dos.a.all>`.

      This generates DOS and AVDOS.

      See
      :ref:`TBT.Atoms.Device.Connect<fdfparam:tbt.atoms.device.connect>`.

      In case any of
      :ref:`TBT.DM.Gf<fdfparam:tbt.dm.gf>`,
      :ref:`TBT.COOP.Gf<fdfparam:tbt.coop.gf>`
      or
      :ref:`TBT.COHP.Gf<fdfparam:tbt.cohp.gf>`
      is **true** this flag will be set to **true** as well.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.dos.a

      fdfparam:TBT.DOS!A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.DOS.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the DOS from the spectral function. This will not
      calculate the DOS from the last electrode (last in the list
      :ref:`TBT.Elecs<fdfparam:tbt.elecs>`),
      see
      :ref:`TBT.DOS.A.All<fdfparam:tbt.dos.a.all>`.

      Its relation to the Green function DOS can be inferred from
      Eq. `[eq:dos-gf] <#eq:dos-gf>`__ (see
      :ref:`TBT.DOS.Gf<fdfparam:tbt.dos.gf>`).
      If there are no bound states in the device region then prefer this
      option and
      :ref:`TBT.DOS.A.All<fdfparam:tbt.dos.a.all>`.

      This generates ADOS\_<> and AVADOS\_<>.

      See
      :ref:`TBT.Atoms.Device.Connect<fdfparam:tbt.atoms.device.connect>`.

      In case any of
      :ref:`TBT.Current.Orb<fdfparam:tbt.current.orb>`,
      :ref:`TBT.DM.A<fdfparam:tbt.dm.a>`,
      :ref:`TBT.COOP.A<fdfparam:tbt.coop.a>`
      or
      :ref:`TBT.COHP.A<fdfparam:tbt.cohp.a>`
      is **true** this flag will be set to **true** as well.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.dos.a.all

      fdfparam:TBT.DOS!A.All

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.DOS.A.All

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the DOS from the spectral function and do so with *all*
      electrodes.

      This additionally generates ADOS\_<> and AVADOS\_<> for the last
      electrode in
      :ref:`TBT.Elecs<fdfparam:tbt.elecs>`.

      **NOTE:** if **true**, this implicitly sets
      :ref:`TBT.DOS.A<fdfparam:tbt.dos.a>`
      to **true**.

Setting the flags
:ref:`TBT.DOS.Gf<fdfparam:tbt.dos.gf>`
and
:ref:`TBT.DOS.A.All<fdfparam:tbt.dos.a.all>`
to **true** enables the estimation of bound states in the scattering
region via this simple expression

.. math::

   \rho_{\mathrm{bound-states}} = \rho_{\textbf{G}} - 
     \sum_i \rho_{\textbf{A}_i},

where the sum is over all electrodes, :math:`\textbf{G}` and
:math:`\textbf{A}_i` are the Green and spectral function, respectively.
Note that typically :math:`\rho_{\mathrm{bound-states}}=0`.

The below two options enables the calculation of the energy resolved
density matrices. In effect they may be used to construct
:math:`\mathrm{LDOS}(E)` profiles using sisl.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.dm.gf

      fdfparam:TBT.DM!Gf

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.DM.Gf

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the energy and :math:`k`-resolved density matrix for the
      Green function. The density matrix may be used to construct
      real-space LDOS profiles.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.dm.a

      fdfparam:TBT.DM!A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.DM.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the energy and :math:`k`-resolved density matrix for the
      electrode spectral functions. The density matrix may be used to
      construct real-space LDOS profiles.

In addition to the DOS analysis of the Green and spectral functions, the
Crystal Orbital Overlap Population and Crystal Orbital Hamilton
Population may also be calculated. These are only available if
TBtrans is compiled with NetCDF-4 support.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.coop.gf

      fdfparam:TBT.COOP!Gf

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.COOP.Gf

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate COOP from the Green function in the device region.

      The COOP curve is calculated as:

      .. math::



             \mathrm{COOP}_{\mu\nu} = \frac{-1}\pi\Im[\textbf{G}_{\mu\nu} \textbf{S}_{\nu\mu}].

      The COOP curves are orbital, energy and :math:`k`-resolved and
      they may thus result in very large output files.

      **NOTE:** Untested!

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.coop.a

      fdfparam:TBT.COOP!A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.COOP.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate COOP from the spectral function in the device region.

      The COOP curve is calculated as:

      .. math::



             \mathrm{COOP}_{\mu\nu} = \frac{1}{2\pi}\Re[\textbf{A}_{\mu\nu} \textbf{S}_{\nu\mu}].

      The COOP curves are orbital, energy and :math:`k`-resolved and
      they may thus result in very large output files.

      **NOTE:** Untested!

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.cohp.gf

      fdfparam:TBT.COHP!Gf

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.COHP.Gf

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate COHP from the Green function in the device region.

      The COHP curve is calculated as:

      .. math::



             \mathrm{COHP}_{\mu\nu} = \frac{-1}\pi\Im[\textbf{G}_{\mu\nu} \textbf{H}_{\nu\mu}].

      The COHP curves are orbital, energy and :math:`k`-resolved and
      they may thus result in very large output files.

      **NOTE:** Untested!

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.cohp.a

      fdfparam:TBT.COHP!A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.COHP.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate COHP from the spectral function in the device region.

      The COHP curve is calculated as:

      .. math::



             \mathrm{COHP}_{\mu\nu} = \frac{1}{2\pi}\Re[\textbf{A}_{\mu\nu} \textbf{H}_{\nu\mu}].

      The COHP curves are orbital, energy and :math:`k`-resolved and
      they may thus result in very large output files.

      **NOTE:** Untested!

.. container:: fdfentrycontainer fdfentry-integer

   .. container:: labelcontainer
      :name: fdfparam:tbt.t.eig

      fdfparam:TBT.T!Eig

   .. container:: fdfparamtype

      integer

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.T.Eig

      .. container:: fdfparamdefault

         0

   .. container:: fdfentrycontainerbody

      Specify how many of the transmission eigenvalues will be
      calculated.

      This generates TEIG\_\_ and AVTEIG\_\_, possibly CEIG\_ and
      AVCEIG\_. The former is for two different electrodes
      :math:`i\neq j`, while the latter is for electrode :math:`i=j`.

      **NOTE:** if you specify a number of eigenvalues above the
      available number of eigenvalues, TBtrans will automatically
      truncate it to a reasonable number.

      **NOTE:** The transmission eigenvalues for :math:`N>2` systems is
      not fully understood and the transmission eigenvalues calculated
      in TBtrans is done by diagonalizing this sub-matrix:

      .. math:: \textbf{G} \boldsymbol \Gamma_i \textbf{G}^\dagger \boldsymbol \Gamma_j.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.t.all

      fdfparam:TBT.T!All

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.T.All

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      By default TBtrans only calculates transmissions in *one*
      direction because time-reversal symmetry makes
      :math:`T_{ij}=T_{ji}`. If one wishes to assert this, or if
      time-reversal symmetry does not apply for your system, one may set
      this to **true** to explicitly calculate all transmissions.

      This additionally generates TRANS\_\_ and AVTRANS\_\_ for all
      electrode combinations (and the equivalent eigenvalue files if
      :ref:`TBT.T.Eig<fdfparam:tbt.t.eig>`
      is **true**.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.t.out

      fdfparam:TBT.T!Out

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.T.Out

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      The total transmission out of any electrode [1]_ may easily be
      calculated using only the scattering matrix of the origin
      electrode and the scattering region Green function. This enables
      the calculation of these equations

      .. math::

         \begin{aligned}
             i\mathop{\mathrm{Tr}}[(\textbf{G} - \textbf{G}^\dagger)\boldsymbol \Gamma_j],
             
             

             \\
             \mathop{\mathrm{Tr}}[\textbf{G} \boldsymbol \Gamma_j \textbf{G}^\dagger\boldsymbol
             \Gamma_j].
             
             

           
         \end{aligned}

      The total transmission out of electrode :math:`j` may then be
      calculated as

      .. math::



             T_j = i\mathop{\mathrm{Tr}}[(\textbf{G} - \textbf{G}^\dagger)\boldsymbol \Gamma_j] 
             -
             \mathop{\mathrm{Tr}}[\textbf{G} \boldsymbol \Gamma_j \textbf{G}^\dagger\boldsymbol \Gamma_j].

      This generates two sets of files: CORR\_<> and TRANS\_\_ which
      corresponds to equations Eqs. `[eq:G.Gamma] <#eq:G.Gamma>`__ and
      `[eq:G.Gamma.G.Gamma] <#eq:G.Gamma.G.Gamma>`__, respectively. To
      calculate :math:`T_j` subtract the two files according to
      Eq. `[eq:T-out] <#eq:T-out>`__.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.current.orb

      fdfparam:TBT.Current!Orb

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Current.Orb

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Whether the orbital currents will be calculated and stored. These
      will be stored in a sparse matrix format corresponding to the
      SIESTA sparse format with only the device atoms in the sparse
      pattern.

      Orbital currents are implemented as:

      .. math::



             J_{\alpha \beta}(E) = i [
             (\textbf{H}_{\beta\alpha} - E\textbf{S}_{\beta\alpha}) \textbf{A}_{\alpha\beta}(E)
             - 
             (\textbf{H}_{\alpha\beta} - E\textbf{S}_{\alpha\beta}) \textbf{A}_{\beta\alpha}(E)],

      where we have left out the pre-factor (:math:`e/\hbar`)
      intentionally. sisl may be used to analyze the orbital currents
      and enables easy transformation of orbital currents to bond
      currents and activity currents(N. Papior et al. 2017).

      **NOTE:** this requires TBtrans to be compiled with NetCDF-4
      support, see
      Sec. :ref:`arch-make<sec:arch-make>`.

.. container:: fdfentrycontainer fdfentry-integer

   .. container:: labelcontainer
      :name: fdfparam:tbt.spin

      fdfparam:TBT.Spin

   .. container:: fdfparamtype

      integer

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Spin

      .. container:: fdfparamdefault

         <all>

   .. container:: fdfentrycontainerbody

      If the Hamiltonian is a polarized calculation one my define the
      index of the spin to be calculated.

      This allows one to simultaneously calculate the spin-up and
      spin-down transmissions, for instance

      ::

           $ tbtrans -fdf TBT.Spin:1 -D UP RUN.fdf &
           $ tbtrans -fdf TBT.Spin:2 -D DOWN RUN.fdf &

      which will create two folders ``UP`` and ``DOWN`` and output the
      relevant physical quantities in the respective folders.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.symmetry.timereversal

      fdfparam:TBT.Symmetry!TimeReversal

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Symmetry.TimeReversal

      .. container:: fdfparamdefault

         true

   .. container:: fdfentrycontainerbody

      Whether the Hamiltonian and the calculation should use
      time-reversal symmetry. Currently this only affects
      :math:`\textbf{k}`-point sampling calculations by not removing any
      symmetry :math:`\textbf{k}`-points.

      If one has :math:`\textbf{k}`-point sampling and wishes to use
      :ref:`TBT.Current.Orb<fdfparam:tbt.current.orb>`
      this should be **false**.

Device region
~~~~~~~~~~~~~

The scattering region (and thus device region) is formally consisting of
all atoms besides the electrodes. However, when calculating the
transmission this choice is very inefficient. Thus to heavily increase
throughput one may define a smaller device region consisting of a subset
of atoms in the scattering region.

The choice of atoms *must* separate each electrode from each other.
TBtrans will stop if this is not enforced.

Remark that the physical quantities such as DOS, spectral DOS, orbital
currents may only be calculated in the selected device region.

.. container:: fdfentrycontainer fdfentry-block/list

   .. container:: labelcontainer
      :name: fdfparam:tbt.atoms.device

      fdfparam:TBT.Atoms!Device

   .. container:: fdfparamtype

      block/list

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Atoms.Device

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      <<all but electrodes>>

      This flag may either be a block, or a list.

      A block with each line denoting the atoms that consists of the
      device region.

      ::

             %block TBT.Atoms.Device
                atom [ 10 -- 20 ]
                atom [ 30 -- 40 ]
                # Atoms removed from the device region
                # Even though they are specified in other
                # lines
                not-atom [ 15, 35]
             % endblock
             # Or equivalently as a list   
             TBT.Atoms.Device [10 -- 14, 16 -- 20, 30 -- 34, 36 -- 40]

      will limit the device region to atoms [10–14, 16–20, 30–34,
      36–40].

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.atoms.device.connect

      fdfparam:TBT.Atoms!Device.Connect

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Atoms.Device.Connect

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Setting this to **true** will *extend* the device region to also
      include atoms that the input device atoms has matrix elements
      between. This may be important when using non-orthogonal basis
      sets as one can ensure the full overlap matrix on the selected
      device atoms.

      **NOTE:** this parameter should be set to **true** in case
      accurate DOS calculations are required on the specified device
      atoms (if using a non-orthogonal basis set).

.. container:: fdfentrycontainer fdfentry-block/list

   .. container:: labelcontainer
      :name: fdfparam:tbt.atoms.buffer

      fdfparam:TBT.Atoms!Buffer

   .. container:: fdfparamtype

      block/list

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Atoms.Buffer

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      A block with each line denoting the atoms that are disregarded in
      the Green function calculation. For self-consistent calculations
      it may be required to introduce buffer atoms which are removed
      from the SCF cycle. In such cases these atoms should also be
      removed from the transport calculation.

      ::

             %block TBT.Atoms.Buffer
                atom [ 1 -- 5 ]
             %endblock
             # Or equivalently as a list   
             TBT.Atoms.Buffer [1 -- 5]

      will remove atoms [1–5] from the calculation.

Brillouin zone
~~~~~~~~~~~~~~

TBtrans allows calculating physical quantities via averaging in the
Brillouin zone.

.. container:: fdfentrycontainer fdfentry-list/block

   .. container:: labelcontainer
      :name: fdfparam:tbt.k

      fdfparam:TBT.k

   .. container:: fdfparamtype

      list/block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.k

      .. container:: fdfparamdefault

         <kgrid_Monkhorst_Pack>

   .. container:: fdfentrycontainerbody

      If
      :ref:`TBT.k.File<fdfparam:tbt.k.file>`
      is present, that will have precedence.

      Specify how to perform Brillouin zone integrations.

      This may be given as a list like this:

      ::

             TBT.k [A B C]

      where each integer corresponds to the diagonal elements of the
      Monkhorst-Pack grid. I.e.

      ::

             TBT.k [10 10 1]
             %block TBT.k
               10  0 0 0.
                0 10 0 0.
                0  0 1 0.
             %endblock 

      are equivalent.

      If you supply this flag as a block the following options are
      available:

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            path

         .. container:: labelcontainer
            :name: fdfparam:tbt.k.path

            fdfparam:TBT.k!path

         Define a Brillouin zone path [2]_ where the :math:`k`-points
         are equi-spaced. It may be best described using this example:

         ::

              path 10
                from 0.  0.  0.
                to   0.5 0.5 0.
              path 20
                from 0.25 0.25 0.
                to   0.0  0.5  0.

         This will create :math:`k`-points starting from the
         :math:`\Gamma`-point and move to the Brillouin zone boundary at
         [:math:`1/2`, :math:`1/2`, :math:`0`] with spacing to have 10
         points.

         There is no requirement that the paths are connected and one
         may specify as many paths as wanted.

         .. container:: fdfoptionscontainer

            .. container:: optioncontainer

               even-path

            It is generally advised to add this flag in the blog
            (somewhere) if one wants equi-distance :math:`k`-spacings in
            the Brillouin zone. This flag sums up the total number of
            :math:`k`-points on the total path and then calculates the
            exact number of required points required on each path to
            have the same :math:`\delta
                  k` in each path.

         **NOTE:** if any one path is found in the block the options
         (explained below) are ignored.

         .. container:: optioncontainer

            diagonal|diag

         .. container:: labelcontainer
            :name: fdfparam:tbt.k.diagonal

            fdfparam:TBT.k!diagonal

         Specify the number of :math:`k` points in each unit-cell
         direction

         diagonal 3 3 1 will use 3 :math:`k` points along the first and
         second lattice vectors and only one along the third lattice
         vector.

         .. container:: optioncontainer

            displacement|displ

         .. container:: labelcontainer
            :name: fdfparam:tbt.k.displacement

            fdfparam:TBT.k!displacement

         Specify the displacement of the Brillouin zone :math:`k` points
         along each lattice vector. This input is similar to diagonal
         but requires real input.

         displacement 0.5 0.25 0. will displace the first and second
         :math:`k` origin to [:math:`1/2`, :math:`1/4`, :math:`0`].

         .. container:: optioncontainer

            size

         .. container:: labelcontainer
            :name: fdfparam:tbt.k.size

            fdfparam:TBT.k!size

         This reduces the sampled Brillouin zone to only the fractional
         size of each lattice vector direction.

         This may be used to only sample :math:`k`-points in a reduced
         Brillouin zone which for instance is useful if one wishes to
         sample the Dirac point in graphene in an energy range of
         :math:`-0.5\,\mathrm{eV}` – :math:`0.5\,\mathrm{eV}`.

         size 0.5 1. 1. will reduce the sampled :math:`k` points along
         the first reciprocal lattice to be in the range
         ]\ :math:`-1/4`, :math:`1/4`], while the other directions are
         still sampled ]\ :math:`-1/2`, :math:`1/2`].

         **NOTE:** expert use only.

         .. container:: optioncontainer

            list

         .. container:: labelcontainer
            :name: fdfparam:tbt.k.list

            fdfparam:TBT.k!list

         Explicitly specify the sampled :math:`k`-points and
         (optionally) the associated weights.

         ::

              list 2
                0.  0.  0.  0.5
                0.5 0.5 0.

         where the integer on the list line specifies the number of
         lines that contains :math:`k` points. Each line *must* be
         created with :math:`3` reals which define the :math:`k` point
         in units of the reciprocal lattice vectors
         (]\ :math:`-1/2`–:math:`1/2`]).

         An optional 4th value denote the associated weight which is
         defaulted to :math:`1/N` where :math:`N` is the total number of
         :math:`k` points.

         **NOTE:** if this is found it will neglect the other input
         options (except path).

         .. container:: optioncontainer

            method

         .. container:: labelcontainer
            :name: fdfparam:tbt.k.method

            fdfparam:TBT.k!method

         Define how the :math:`k`-points should be created in the
         Brillouin zone.

         Currently these options are available (Monkhorst-Pack being the
         default)

         .. container:: fdfoptionscontainer

            .. container:: optioncontainer

               Monkhorst-Pack|MP

            .. container:: labelcontainer
               :name: fdfparam:tbt.k.method.monkhorst-pack

               fdfparam:TBT.k!method!Monkhorst-Pack

            Use the regular Monkhorst-Pack sampling (equi-spaced) with
            simple linear weights.

            .. container:: optioncontainer

               Gauss-Legendre

            .. container:: labelcontainer
               :name: fdfparam:tbt.k.method.gauss-legendre

               fdfparam:TBT.k!method!Gauss-Legendre

            Use the Gauss-Legendre quadrature and weights for
            constructing the :math:`k` points and weights. These
            :math:`k` points are not equi-spaced and puts more weight to
            the :math:`\Gamma` point.

            .. container:: optioncontainer

               Simpson-mix

            .. container:: labelcontainer
               :name: fdfparam:tbt.k.method.simpson-mix

               fdfparam:TBT.k!method!Simpson-mix

            Use the Newton-Cotes method (Simpson, degree 3) which uses
            equi-spaced points but non-uniform weights.

            .. container:: optioncontainer

               Boole-mix

            .. container:: labelcontainer
               :name: fdfparam:tbt.k.method.boole-mix

               fdfparam:TBT.k!method!Boole-mix

            Use the Newton-Cotes method (Boole, degree 5) which uses
            equi-spaced points but non-uniform weights.

         .. container:: optioncontainer

            <Value of
            :ref:`*<fdfparam:\*>`>siesta-method

         One may also use the typical kgrid_Monkhorst_Pack method of
         input as done in SIESTA. This is a :math:`3\times3` block such
         as:

         ::

              10  0  0 0.
               0 15  0 0.
               0  0  1 0.

         which uses :math:`10`, :math:`15` and :math:`1`
         :math:`k`-points along the 1st, 2nd and 3rd reciprocal lattice
         vectors. And with :math:`0` displacement.

         **NOTE:** it is recommended to use the diagonal option unless
         off-diagonal :math:`k` points are needed.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.k.file

      fdfparam:TBT.k.File

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.k.File

      .. container:: fdfparamdefault

         NONE

   .. container:: fdfentrycontainerbody

      If provided, it is a (relative) path to a file containing
      :math:`k`-points in the filename containing points in units of the
      reciprocal lattice vectors. I.e. in the range of
      :math:`]-1/2 ; 1/2]`. See e.g.
      :ref:`TBT.k.list<fdfparam:tbt.k.list>`
      for details, or see kgrid.File in the SIESTA manual for more
      details about the exact file layout.

      The only exception is that the weights need not sum to :math:`1`
      as is the case for SIESTA. This is because TBtrans can do reduced
      Brillouin zone calculations. Hence, be careful about what you
      pass.

Energy grid
~~~~~~~~~~~

TBtrans uses a default energy reference as the Fermi level in the
corresponding TranSIESTA calculation. I.e. the equilibrium Fermi level.
Thus one should be aware when using a shifted bias window that the
calculated properties shifts according to the applied bias. For example;
if one performs two equivalent 2-terminal calculations A) with
:math:`\mu_L=V`, :math:`\mu_R=0` and the other B) with
:math:`\mu_L=V/2`, :math:`\mu_R=-V/2` then the calculated properties are
equivalent if one shifts the energy spectrum of A) by
:math:`E \to E - V/2`. Any 2-terminal calculation is recommended to be
setup with :math:`\mu_L=V/2` and :math:`\mu_R=-V/2` due to the fixed
energy reference, :math:`E_R = 0`.

The Green function is calculated at explicit energies and does not rely
on diagonalization routines to retrieve the eigenspectrum. This is due
to the smearing of states from the coupling with the semi-infinite
electrodes.

It is thus important to define an energy grid for analysis of the DOS
and transmission.

.. container:: fdfentrycontainer fdfentry-energy

   .. container:: labelcontainer
      :name: fdfparam:tbt.contours.eta

      fdfparam:TBT.Contours!Eta

   .. container:: fdfparamtype

      energy

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Contours.Eta

      .. container:: fdfparamdefault

         :math:`\operatorname{min}{\eta_{\mathfrak e}}{}/10`

   .. container:: fdfentrycontainerbody

      The imaginary (:math:`\eta`) part of the Green function in the
      device region. Note that the electrodes imaginary part may be
      controlled via
      :ref:`TBT.Elecs.Eta<fdfparam:tbt.elecs.eta>`.

      This value controls the smearing of the DOS on the energy axis.
      Generally one need not take into account :math:`\eta` values
      different from 0. However, in cases where localized states are
      found a smearing in the device region can help numerics. Therefore
      it defaults to :math:`\operatorname{min}[\eta_{\mathfrak e}]/10`.
      This ensures that the device broadening is always smaller than the
      electrodes while allowing broadening of localized states.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.contours

      fdfparam:TBT.Contours

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Contours

      .. container:: fdfparamdefault

         *see note further down*

   .. container:: fdfentrycontainerbody

      Each line in this block corresponds to a specific contour.
      Enabling several lines of input allows to create regions of the
      energy grid which has a high density and ranges of energies with
      lower density. Also it allows to bypass energy ranges where the
      DOS is zero in for instance a semi-conductor.

      See
      :ref:`TBT.Contour.\<\><fdfparam:tbt.contour.\<\>>`
      for details on specifying the energy contour.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.contour.<>

      fdfparam:TBT.Contour!<>

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Contour.<>

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      Specify a contour named <> with options within the block.

      The names <> are taken from the
      :ref:`TBT.Contours<fdfparam:tbt.contours>`
      block.

      The format of this block is made up of at least :math:`3` lines,
      in the following order of appearance.

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            from *a* to *b*

         .. container:: labelcontainer
            :name: fdfparam:tbt.contour.<>.from

            fdfparam:TBT.Contour!<>!from

         Define the integration range on the energy axis. Thus *a* and
         *b* are energies.

         .. container:: optioncontainer

            points|delta|file

         .. container:: labelcontainer
            :name: fdfparam:tbt.contour.<>.points

            fdfparam:TBT.Contour!<>!points

         .. container:: labelcontainer
            :name: fdfparam:tbt.contour.<>.delta

            fdfparam:TBT.Contour!<>!delta

         .. container:: labelcontainer
            :name: fdfparam:tbt.contour.<>.file

            fdfparam:TBT.Contour!<>!file

         Define the number of integration points/energy separation. If
         specifying the number of points an integer should be supplied.

         If specifying the separation between consecutive points an
         energy should be supplied (e.g. 0.01 eV).

         Optionally one may specify a file which contains the energy
         points and their weights.

         This file has the same formatting as the TBT.CC output with
         some optional inputs. Below is an example input file.

         ::

            [fontsize=\footnotesize]
            # There are 2 different input options:
            # 1. Re[E] Im[E] W  (optional unit)
            # 2. Re[E] W        (optional unit) (imaginary part will be device Eta)
            # If the unit is specified on any line, all subsequent lines will use
            # the specified unit. Default unit is eV!
            # Empty lines and lines starting with # will be ignored.
            -0.5  0.1    # E = -0.5 eV, weight (for integrating current) of 0.1 eV
            -0.01 0.1 Ry # E = -0.01 Ry and weight 0.1 Ry
            -0.02 0.1    # E = -0.02 Ry (above unit continue) and weight 0.1 Ry
            -0.2  0.1 eV # E = -0.2 eV and weight 0.1 eV
            -0.2 1. 0.1  # E = -0.2 eV and 1. eV eta and weight 0.1 eV

         If the file specified is TBT.CC the same energy points will be
         used. Note that the resulting TBT.nc file does not store the
         energies as complex numbers, thus one cannot subsequently
         extract the :math:`\eta` value used for the individual energy
         points.

         **NOTE:** for PHtrans the energies will be squared internally
         to be in correct units, hence the units should still be
         :math:`\mathrm{eV}`.

         .. container:: optioncontainer

            method

         .. container:: labelcontainer
            :name: fdfparam:tbt.contour.<>.method

            fdfparam:TBT.Contour!<>!method

         Specify the numerical method used to conduct the integration.
         Here a number of different numerical integration schemes are
         accessible

         .. container:: fdfoptionscontainer

            .. container:: optioncontainer

               mid|mid-rule

            Use the mid-rule for integration.

            .. container:: optioncontainer

               simpson|simpson-mix

            Use the composite Simpson :math:`3/8` rule (three point
            Newton-Cotes).

            .. container:: optioncontainer

               boole|boole-mix

            Use the composite Booles rule (five point Newton-Cotes).

            .. container:: optioncontainer

               G-legendre

            Gauss-Legendre quadrature.

            .. container:: optioncontainer

               tanh-sinh

            Tanh-Sinh quadrature.

            **NOTE:** has opt precision <>.

            .. container:: optioncontainer

               user

            User defined input via a file.

         .. container:: optioncontainer

            opt

         .. container:: labelcontainer
            :name: fdfparam:tbt.contour.<>.opt

            fdfparam:TBT.Contour!<>!opt

         Specify additional options for the method. Only a selected
         subset of the methods have additional options.

By default the TBtrans energy grid is defined as

::

     TBT.Contours.Eta 0. eV
     %block TBT.Contours
       line
     %endblock
     %block TBT.Contour.line
        from -2. eV to 2. eV
         delta 0.01 eV
           method mid-rule
     %endblock

An example of input using a file (note that regular contour setups may
be used together with file-inputs)

::

     TBT.Contours.Eta 0. eV
     %block TBT.Contours
       file
     %endblock
     %block TBT.Contour.file
        from 2. eV to 2.5 eV
         file my_energies
     %endblock

Note that the energy specifications are necessary (due to internal
bookkeeping).

Chemical potentials
-------------------

For :math:`N` electrodes there will also be :math:`N_\mu` chemical
potentials. They are defined via blocks similar to
:ref:`TBT.Elecs<fdfparam:tbt.elecs>`.
If no bias is applied TBtrans will default to a single chemical
potential with the chemical potential in equilibrium. In this case you
need not specify any chemical potentials.

By default TBtrans creates a single chemical potential with the chemical
potential equal to the device Fermi-level. Hence, performing non-bias
calculations does not require one to specify these blocks.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.chempots

      fdfparam:TBT.ChemPots

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.ChemPots

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      Each line denotes a new chemical potential which may is further
      defined in the
      :ref:`TBT.ChemPot.\<\><fdfparam:tbt.chempot.\<\>>`
      block.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.chempot.<>

      fdfparam:TBT.ChemPot!<>

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.ChemPot.<>

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      Each line defines a setting for the chemical potential named <>.

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            chemical-shift|mu

         .. container:: labelcontainer
            :name: fdfparam:tbt.chempot.<>.chemical-shift

            fdfparam:TBT.ChemPot!<>!chemical-shift

         .. container:: labelcontainer
            :name: fdfparam:tbt.chempot.<>.mu

            fdfparam:TBT.ChemPot!<>!mu

         Define the chemical shift (an energy) for this chemical
         potential. One may specify the shift in terms of the applied
         bias using V/<integer> instead of explicitly typing the energy.

         .. container:: optioncontainer

            ElectronicTemperature|Temp|kT

         .. container:: labelcontainer
            :name: fdfparam:tbt.chempot.<>.electronictemperature

            fdfparam:TBT.ChemPot.<>!ElectronicTemperature

         .. container:: labelcontainer
            :name: fdfparam:tbt.chempot.<>.temp

            fdfparam:TBT.ChemPot.<>!Temp

         .. container:: labelcontainer
            :name: fdfparam:tbt.chempot.<>.kt

            fdfparam:TBT.ChemPot.<>!kT

         Specify the electronic temperature (as an energy or in Kelvin).
         This defaults to TS.ElectronicTemperature.

         One may specify this in units of TS.ElectronicTemperature by
         using the unit kT.

      It is important to realize that the parameterization of the
      voltage into the chemical potentials enables one to have a
      *single* input file which is never required to be changed, even
      when changing the applied bias.

These options complicate the input sequence for regular :math:`2`
electrode which is unfortunate.

Electrode configuration
-----------------------

The electrodes are defining the semi-infinite region that is coupled to
the scattering region.

TBtrans is a fully :math:`N` electrode calculator. Thus the input for
such setups is rather complicated.

TBtrans defaults to read the TranSIESTA electrodes and as such one may
replace TBT by TS and TBtrans will still work. However, the TBT has
precedence.

If there is only :math:`1` chemical potential all electrodes will
default to use this chemical potential, thus for non-bias calculations
there is no need to specify the chemical potential
(:ref:`TBT.Elec.\<\>.chemical-potential<fdfparam:tbt.elec.\<\>.chemical-potential>`).

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs

      fdfparam:TBT.Elecs

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      Each line denote an electrode which may be queried in
      :ref:`TBT.Elec.\<\><fdfparam:tbt.elec.\<\>>`
      for its setup.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.elec.<>

      fdfparam:TBT.Elec.<>

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elec.<>

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      Each line represents a setting for electrode <>. There are a few
      lines that *must* be present, HS, semi-inf-dir, electrode-pos,
      chem-pot (only if
      :ref:`TBT.Voltage<fdfparam:tbt.voltage>`
      is not :math:`0`).

      If there are some settings that you only want to take effect in
      TBtrans calculations you can prefix the option with tbt.Eta, e.g.
      which will only be used for the TBtrans calculations. Note that
      *all* tbt.\* options must be located at the end of the block. This
      may be particularly useful with respect to the Green function file
      options.

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            HS

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.hs

            fdfparam:TBT!Elec.<>!HS

         The electronic structure information from the initial electrode
         calculation. This file retains the geometrical information as
         well as the Hamiltonian, overlap matrix and the Fermi-level of
         the electrode. This is a file-path and the electrode TSHS need
         not be located in the simulation folder.

         TBtrans also reads NetCDF4 files which contain the electronic
         structure. This may be created using sisl.

         **NOTE:** Please note that TranSIESTA expects a metallic
         electrode. Results can not be trusted for semi-conductors.

         .. container:: optioncontainer

            semi-inf-direction|semi-inf-dir|semi-inf

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.semi-inf-direction

            fdfparam:TBT.Elec.<>!semi-inf-direction

         The semi-infinite direction of the electrode with respect to
         the electrode unit-cell.

         It may be one of [-+][abc], [-+]A[123], ab, ac, bc or abc. The
         latter four all describe a real-space self-energy as described
         in (N. Papior et al. 2019).

         **NOTE:** this has nothing to do with the scattering region
         unit cell, TBtrans will figure out the alignment of the
         electrode unit-cell and the scattering region unit-cell.

         .. container:: optioncontainer

            chemical-potential|chem-pot|mu

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.chemical-potential

            fdfparam:TBT.Elec.<>!chemical-potential

         The chemical potential that is associated with this electrode.
         This is a string that should be present in the
         :ref:`TBT.ChemPots<fdfparam:tbt.chempots>`
         block in case there is a bias applied in the calculation.

         .. container:: optioncontainer

            electrode-position|elec-pos

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.electrode-position

            fdfparam:TBT.Elec.<>!electrode-position

         The index of the electrode in the scattering region. This may
         be given by either elec-pos <idx>, which refers to the first
         atomic index of the electrode residing at index <idx>. Else the
         electrode position may be given via elec-pos end <idx> where
         the last index of the electrode will be located at <idx>.

         .. container:: optioncontainer

            used-atoms

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.used-atoms

            fdfparam:TBT.Elec.<>!used-atoms

         Number of atoms from the electrode calculation that is used in
         the scattering region as electrode. This may be useful when the
         periodicity of the electrodes forces extensive electrodes in
         the semi-infinite direction.

         If the semi-infinite direction is *positive*, the first atoms
         will be retained. Contrary, if the semi-infinite direction is
         *negative*, the last atoms will be retained.

         **NOTE:** do not set this if you use all atoms in the
         electrode.

         .. container:: optioncontainer

            Bulk

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.bulk

            fdfparam:TBT.Elec.<>!Bulk

         Logical controlling whether the Hamiltonian of the electrode
         region in the scattering region is enforced *bulk* or whether
         the Hamiltonian is taken from the scattering region elements.

         .. container:: optioncontainer

            Eta

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.eta

            fdfparam:TBT.Elec.<>!Eta

         Control the imaginary energy (:math:`\eta`) of the surface
         Green function for this electrode.

         **NOTE:** if this energy is negative the complex value
         associated with the contour is used. This is particularly
         useful when providing a user-defined contour. Ensure that all
         imaginary values are larger than :math:`0` as otherwise
         TBtrans may seg-fault.

         **NOTE:** for PHtrans calculations you are highly encouraged to
         change this value since the default (:math:`1\,\mathrm{meV}`)
         is very low.

         .. container:: optioncontainer

            Bloch

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.bloch

            fdfparam:TBT.Elec.<>!Bloch

         :math:`3` integers are present on this line which each denote
         the number of times bigger the scattering region electrode is
         compared to the electrode, in each lattice direction. Remark
         that these expansion coefficients are with regard to the
         electrode unit-cell. This is denoted “Bloch” because it is an
         expansion based on Bloch waves.

         Please see *Matching electrode coordinates: basic rules* in the
         SIESTA manual for details.

         .. container:: optioncontainer

            Bloch-A/a1|B/a2|C/a3

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.bloch

            fdfparam:TBT.Elec.<>!Bloch

         Specific Bloch expansions in each of the electrode unit-cell
         direction. See Bloch for details.

         .. container:: optioncontainer

            out-of-core

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.out-of-core

            fdfparam:TBT.Elec.<>!out-of-core

         If **true** the GF files are created which contain the surface
         Green function. Setting this to **true** may be advantageous
         when performing many calculations using the same :math:`k` and
         energy grid using the same electrode. In those case this will
         heavily increase throughput. If **false** (default) the surface
         Green function will be calculated when needed.

         **NOTE:** simultaneous calculations may read the same GF file.

         .. container:: optioncontainer

            tbt.Gf/Gf

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.gf

            fdfparam:TBT.Elec.<>!Gf

         String with filename of the surface Green function data. This
         may be used to place a common surface Green function file in a
         top directory which may then be used in all calculations using
         the same electrode and the same contour. If doing many
         calculations with the same electrode and :math:`\textbf{k}`,
         :math:`E` grids, then this can greatly improve throughput. It
         has a cost of disk-space. Note that the energy-grids are
         dependent on the applied bias.

         .. container:: optioncontainer

            Gf-Reuse

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.gf-reuse

            fdfparam:TBT.Elec.<>!Gf-Reuse

         Logical deciding whether the surface Green function file should
         be re-used or deleted. If this is **false** the surface Green
         function file is deleted and re-created upon start.

         .. container:: optioncontainer

            pre-expand

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.pre-expand

            fdfparam:TBT.Elec.<>!pre-expand

         String denoting how the expansion of the surface Green function
         file will be performed. This only affects the Green function
         file if Bloch is larger than 1. By default the Green function
         file will contain the fully expanded surface Green function,
         but not Hamiltonian and overlap matrices (Green). One may
         reduce the file size by setting this to Green which only
         expands the surface Green function. Finally none may be passed
         to reduce the file size to the bare minimum. For performance
         reasons all is preferred.

         .. container:: optioncontainer

            Accuracy

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.accuracy

            fdfparam:TBT.Elec.<>!Accuracy

         Control the convergence accuracy required for the self-energy
         calculation when using the Lopez-Sanchez, Lopez-Sanchez
         iterative scheme.

         **NOTE:** advanced use *only*.

         .. container:: optioncontainer

            delta-Ef

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.delta-ef

            fdfparam:TBT!Elec.<>!delta-Ef

         Specify an offset for the Fermi-level of the electrode. This
         will directly be added to the Fermi-level found in the
         electrode file.

         **NOTE:** this option only makes sense for semi-conducting
         electrodes since it shifts the entire electronic structure.
         This is because the Fermi-level may be arbitrarily placed
         anywhere in the band gap. It is the users responsibility to
         define a value which does not introduce a potential drop
         between the electrode and device region.

         .. container:: optioncontainer

            V-fraction

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.v-fraction

            fdfparam:TBT!Elec.<>!V-fraction

         Specify the fraction of the chemical potential shift in the
         electrode-device coupling region. This corresponds to:

         .. math::

            \textbf{H}_{\mathfrak eD} \leftarrow \textbf{H}_{\mathfrak eD} +
                  \mu_{\mathfrak e} \mathrm{V}_f \textbf{S}_{\mathfrak eD}

         in the coupling region. Consequently the value *must* be
         between :math:`0` and :math:`1`.

         **NOTE:** this option may be used for tight-binding
         calculations as an empirical applied bias (with the potential
         drop at the electrode/device interface). It *should not* be
         used for converged TranSIESTA calculations.

         .. container:: optioncontainer

            check-kgrid

         .. container:: labelcontainer
            :name: fdfparam:tbt.elec.<>.check-kgrid

            fdfparam:TBT.Elec.<>!check-kgrid

         For :math:`N` electrode calculations the :math:`\textbf{k}`
         mesh will sometimes not be equivalent for the electrodes and
         the device region calculations. However, TBtrans requires that
         the device and electrode :math:`\textbf{k}` samplings are
         commensurate. This flag controls whether this check is
         enforced.

         **NOTE:** only use if fully aware of the implications (for
         tight-binding calculations this may safely be set to
         **false**).

There are several flags which are globally controlling the variables for
the electrodes (with
:ref:`TBT.Elec.\<\><fdfparam:tbt.elec.\<\>>`
taking precedence).

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs.bulk

      fdfparam:TBT.Elecs!Bulk

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs.Bulk

      .. container:: fdfparamdefault

         true

   .. container:: fdfentrycontainerbody

      This globally controls how the Hamiltonian is treated in all
      electrodes. See
      :ref:`TBT.Elec.\<\>.Bulk<fdfparam:tbt.elec.\<\>.bulk>`.

.. container:: fdfentrycontainer fdfentry-energy

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs.eta

      fdfparam:TBT.Elecs!Eta

   .. container:: fdfparamtype

      energy

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs.Eta

      .. container:: fdfparamdefault

         :math:`1\,\mathrm{meV}`

   .. container:: fdfentrycontainerbody

      Globally control the imaginary energy (:math:`\eta`) used for the
      surface Green function calculation. This :math:`\eta` value is
      *not* used in the device region. See
      :ref:`TBT.Elec.\<\>.Eta<fdfparam:tbt.elec.\<\>.eta>`
      for extended details on the usage of this flag.

.. container:: fdfentrycontainer fdfentry-energy

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs.accuracy

      fdfparam:TBT.Elecs!Accuracy

   .. container:: fdfparamtype

      energy

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs.Accuracy

      .. container:: fdfparamdefault

         :math:`10^{-13}\,\mathrm{eV}`

   .. container:: fdfentrycontainerbody

      Globally control the accuracy required for convergence of the
      self-energy. See
      :ref:`TBT.Elec.\<\>.Accuracy<fdfparam:tbt.elec.\<\>.accuracy>`.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs.neglect.principal

      fdfparam:TBT.Elecs!Neglect.Principal

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs.Neglect.Principal

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      If this is **false** TBtrans dies if there are connections beyond
      the principal cell.

      **NOTE:** set this to **true** with care, non-physical results may
      arise. Use at your own risk!

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs.out-of-core

      fdfparam:TBT.Elecs!Out-of-core

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs.Out-of-core

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      This enables reusing the self-energies by storing them on-disk
      (**true**). The surface Green function files may be large files
      but heavily increases throughput if one performs several transport
      calculations using the same electrodes.

      You are encouraged to set this to **true** to reduce computations.
      See
      :ref:`TBT.Elec.\<\>.out-of-core<fdfparam:tbt.elec.\<\>.out-of-core>`.

      Currently this option is not compatible with
      :ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`
      and
      :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`,
      and the bulk transmission and bulk DOS will not be calculated if
      this option is **true**.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs.gf.reuse

      fdfparam:TBT.Elecs!Gf.Reuse

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs.Gf.Reuse

      .. container:: fdfparamdefault

         true

   .. container:: fdfentrycontainerbody

      Globally control whether the surface Green function files should
      be re-used (**true**) or re-created (**false**). See
      :ref:`TBT.Elec.\<\>.Gf-Reuse<fdfparam:tbt.elec.\<\>.gf-reuse>`.

.. container:: fdfentrycontainer fdfentry-length

   .. container:: labelcontainer
      :name: fdfparam:tbt.elecs.coord.eps

      fdfparam:TBT!Elecs!Coord.EPS

   .. container:: fdfparamtype

      length

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Elecs.Coord.EPS

      .. container:: fdfparamdefault

         :math:`10^{-4}\,\mathrm{Bohr}`

   .. container:: fdfentrycontainerbody

      When using Bloch expansion of the self-energies one may experience
      difficulties in obtaining perfectly aligned electrode coordinates.

      This parameter controls how strict the criteria for equivalent
      atomic coordinates is. If TBtrans crashes due to mismatch between
      the electrode atomic coordinates and the scattering region
      calculation, one may increase this criteria. This should only be
      done if one is sure that the atomic coordinates are almost similar
      and that the difference in electronic structures of the two may be
      negligible.

Principal layer interactions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is *extremely* important that the electrodes only interact with one
neighboring supercell due to the self-energy calculation. TBtrans will
print out a block as this

::

    <> principal cell is perfect!

if the electrode is correctly setup and it only interacts with its
neighboring supercell. In case the electrode is erroneously setup,
something similar to the following will be shown in the output file.

::

    <> principal cell is extending out with 96 elements:
       Atom 1 connects with atom 3
       Orbital 8 connects with orbital 26
       Hamiltonian value: |H(8,6587)|@R=-2 =  0.651E-13 eV
       Overlap          :  S(8,6587)|@R=-2 =   0.00    

It is imperative that you have a *perfect* electrode as otherwise
nonphysical results will occur.

Calculation settings
--------------------

The calculation time is currently governed by two things:

#. the size of the device region,

#. and by the partitioning of the block-tri-diagonal matrix.

The first may be controlled via
:ref:`TBT.Atoms.Device<fdfparam:tbt.atoms.device>`.
If one is only interested in transmission coefficients this flag is
encouraged to select the minimum number of atoms that will successfully
run the calculation. Please see the flag entry for further details.

Secondly there is, currently, no way to determine the most optimal
block-partitioning of a banded matrix and TBtrans allows several
algorithms to determine an optimal partitioning scheme. The following
flag controls the partitioning for the device region.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.analyze

      fdfparam:TBT!Analyze

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Analyze

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      As the pivoting algorithm *highly* influences the performance and
      throughput of the transport calculation it is crucial to select
      the best performing algorithm available. This option tells
      TBtrans to analyze the pivoting table for nearly all the
      implemented algorithms and print-out information about them.

      **NOTE:** we advice users to *always* run an analyzation step
      prior to actual calculation and select the *best* BTD format. This
      analyzing step is very fast and can be performed on small
      work-station computers, even on systems of :math:`\gg10,000`
      orbitals.

      To run the analyzing step you may do:

      ::

             tbtrans -fdf TBT.Analyze RUN.fdf > analyze.out

      note that there is little gain on using MPI and it should complete
      within a few minutes, no matter the number of orbitals.

      Choosing the best one may be difficult. Generally one should
      choose the pivoting scheme that uses the least amount of memory.
      However, one should also choose the method with the largest
      block-size being as small as possible. As an example:

      ::

         [fontsize=\footnotesize]
         TBT.BTD.Pivot.Device atom+GPS
         ...
             BTD partitions (7): 
              [ 2984, 2776, 192, 192, 1639, 4050, 105 ]
             BTD matrix block size [max] / [average]: 4050 /   1705.429
             BTD matrix elements in % of full matrix:   47.88707 %

         TBT.BTD.Pivot.Device atom+GGPS
         ...
             BTD partitions (6): 
              [ 2880, 2916, 174, 174, 2884, 2910 ]
             BTD matrix block size [max] / [average]: 2916 /   1989.667
             BTD matrix elements in % of full matrix:   48.62867 %

      Although the GPS method uses the least amount of memory, the GGPS
      will likely perform better as the largest block in GPS is
      :math:`4050` vs. :math:`2916` for the GGPS method.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.btd.optimize

      fdfparam:TBT.BTD!Optimize

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.BTD.Optimize

      .. container:: fdfparamdefault

         speed|memory

   .. container:: fdfentrycontainerbody

      When selecting the smallest blocks for the BTD matrix there are
      certain criteria that may change the size of each block. For very
      memory consuming jobs one may choose the memory.

      **NOTE:** often both methods provide *exactly* the same BTD matrix
      due to constraints on the matrix.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.btd.pivot.device

      fdfparam:TBT.BTD!Pivot.Device

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.BTD.Pivot.Device

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      <atom-<largest overlapping electrode>>

      Decide on the partitioning for the BTD matrix. One may denote
      either atom+ or orb+ as a prefix which does the analysis on the
      atomic sparsity pattern or the full orbital sparsity pattern,
      respectively. If neither are used it will default to atom+.

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            <elec-name>|CG-<elec-name>

         The partitioning will be a connectivity graph starting from the
         electrode denoted by the name. This name *must* be found in the
         :ref:`TBT.Elecs<fdfparam:tbt.elecs>`
         block. One can append more than one electrode to simultaneously
         start from more than 1 electrode. This may be necessary for
         multi-terminal calculations.

         **NOTE:** One may append an optional setting front or fan which
         makes the connectivity graph to consider the geometric front of
         the atoms. For extreme scale simulations or tight-binding
         calculations with constrictions this may improve the BTD matrix
         substantially because it splits the unit-cell into segments of
         equal width.

         .. container:: optioncontainer

            rev-CM

         Use the reverse Cuthill-McKee for pivoting the matrix elements
         to reduce bandwidth. One may omit rev- to use the standard
         Cuthill-McKee algorithm (not recommended).

         This pivoting scheme depends on the initial starting
         electrodes, append +<elec-name> to start the Cuthill-McKee
         algorithm from the specified electrode.

         .. container:: optioncontainer

            GPS

         Use the Gibbs-Poole-Stockmeyer algorithm for reducing the
         bandwidth.

         .. container:: optioncontainer

            GGPS

         Use the generalized Gibbs-Poole-Stockmeyer algorithm for
         reducing the bandwidth.

         .. container:: optioncontainer

            PCG

         Use the perphiral connectivity graph algorithm for reducing the
         bandwidth.

         This pivoting scheme *may* depend on the initial starting
         electrode(s), append +<elec-name> to initialize the PCG
         algorithm from the specified electrode.

      Examples are

      ::

             TBT.BTD.Pivot.Device atom+GGPS
             TBT.BTD.Pivot.Device GGPS
             TBT.BTD.Pivot.Device orb+GGPS
             TBT.BTD.Pivot.Device orb+PCG
             TBT.BTD.Pivot.Device orb+PCG+Left
             TBT.BTD.Pivot.Device orb+rev-CM+Right

      where the first two are equivalent. The 3rd and 4th are more
      heavily on analysis and will typically not improve the bandwidth
      reduction.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.btd.pivot.elec.<>

      fdfparam:TBT.BTD!Pivot.Elec.<>

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.BTD.Pivot.Elec.<>

      .. container:: fdfparamdefault

         atom-<<>>

   .. container:: fdfentrycontainerbody

      If
      :ref:`TBT.Atoms.Device<fdfparam:tbt.atoms.device>`
      has been set to a reduced region the electrode self-energies must
      be *down-folded* through the atoms not part of the device-region.
      In this case these down-fold regions can also be considered a BTD
      matrix which may be optimized separately from the device region
      BTD matrix.

      This option have all available options as described in
      :ref:`TBT.BTD.Pivot.Device<fdfparam:tbt.btd.pivot.device>`
      but one will generally find the best pivoting scheme by using the
      default (atom-<>) which is the atomic connectivity graph from the
      electrode it-self.

      It may be advantageous to use atom-<>-front for very large
      tight-binding calculations where the device region is chosen far
      from this electrode and normal to the electrode-plane.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.btd.spectral

      fdfparam:TBT.BTD!Spectral

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.BTD.Spectral

      .. container:: fdfparamdefault

         propagation|column

   .. container:: fdfentrycontainerbody

      Method used for calculating the spectral function
      (:math:`\textbf{A}_i`). For 4 or more electrodes the column option
      is the default while propagation is the default for less
      electrodes.

      **NOTE:** this option may heavily influence performance. Test for
      a single :math:`k`-point and single energy point to figure out the
      implications of using one over the other.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.btd.pivot.graphviz

      fdfparam:TBT.BTD!Pivot.Graphviz

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.BTD.Pivot.Graphviz

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Create Graphviz [3]_ compatible input files for the pivoting
      tables for all electrodes, TBT.<elec>.gv, and the device, TBT.gv.

      These files may be processed by Graphviz display commands
      ``neato`` etc.

      ::

             neato -x <>
             neato -x -Tpdf <> -o graph.pdf
             neato -x -Tpng <> -o graph.png

Input/Output
------------

TBtrans IO is mainly relying on the NetCDF4 library and full capability
is only achieved if compiled with this library.

Several fdf-flags control how TBtrans performs IO.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.precision

      fdfparam:TBT.CDF!Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.Precision

      .. container:: fdfparamdefault

         single|float|double

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing the quantities in the
      NetCDF4.

      single takes half the disk-space as double and will generally
      retain a sufficient precision of the quantities.

      single and float are equivalent.

      **NOTE:** all calculations are performed using double so this is
      *only* a storage precision.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.dos.precision

      fdfparam:TBT.CDF!DOS.Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.DOS.Precision

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Precision<fdfparam:tbt.cdf.precision>`>

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing DOS in NetCDF4.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.t.precision

      fdfparam:TBT.CDF!T.Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.T.Precision

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Precision<fdfparam:tbt.cdf.precision>`>

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing transmission function in
      NetCDF4.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.t.eig.precision

      fdfparam:TBT.CDF!T.Eig.Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.T.Eig.Precision

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Precision<fdfparam:tbt.cdf.precision>`>

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing transmission eigenvalues in
      NetCDF4.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.current.precision

      fdfparam:TBT.CDF!Current.Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.Current.Precision

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Precision<fdfparam:tbt.cdf.precision>`>

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing orbital current in NetCDF4.

      **NOTE:** This is heavily advised to be in single precision as
      this may easily use large amounts of disk-space if in double
      precision.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.dm.precision

      fdfparam:TBT.CDF!DM.Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.DM.Precision

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Precision<fdfparam:tbt.cdf.precision>`>

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing density matrices in
      NetCDF4.

      **NOTE:** This is heavily advised to be in single precision as
      this may easily use large amounts of disk-space if in double
      precision.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.coop.precision

      fdfparam:TBT.CDF!COOP.Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.COOP.Precision

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Precision<fdfparam:tbt.cdf.precision>`>

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing COOP and COHP curves in
      NetCDF4.

      **NOTE:** This is heavily advised to be in single precision as
      this may easily use large amounts of disk-space if in double
      precision.

.. container:: fdfentrycontainer fdfentry-integer

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.compress

      fdfparam:TBT.CDF!Compress

   .. container:: fdfparamtype

      integer

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.Compress

      .. container:: fdfparamdefault

         0

   .. container:: fdfentrycontainerbody

      Specify whether the NetCDF4 files stored will be compressed. This
      may heavily reduce disk-utilization at the cost of some
      performance.

      This number must be between 0 (no compression) and 9 (maximum
      compression). A higher compression is more time consuming and a
      good compromise between speed and compression is 3.

      **NOTE:** one may subsequently to a TBtrans compilation compress a
      NetCDF4 file using:

      ::

            nccopy -d 3 siesta.TBT.nc newsiesta.TBT.nc

      **NOTE:** one *can not* do parallel I/O together with compression.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.mpi

      fdfparam:TBT.CDF!MPI

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.MPI

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Whether the IO is performed in parallel. If using a large amount
      of MPI processors this may increase performance.

      **NOTE:** the actual performance increase is *very* dependent on
      your hardware support for parallel IO.

      **NOTE:** this automatically sets the compression to 0 (one cannot
      compress and perform parallel IO).

Self-energy
~~~~~~~~~~~

.. container:: labelcontainer
   :name: sec:self-energy

   sec:self-energy

TBtrans enables the storage of the self-energies from the electrodes in
selected regions. I.e. in a two electrode setup the self-energies may be
“down-folded” to a region of interest (say molecule etc.) and then
saved.

This feature enables one to easily use self-energies in Python for
subsequent analysis etc. It is only available if compiled against
NetCDF4.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.selfenergy.save

      fdfparam:TBT!SelfEnergy.Save

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.SelfEnergy.Save

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Store the self-energies of the electrodes. The self-energies are
      first down-folded into the device region (see
      :ref:`TBT.Atoms.Device<fdfparam:tbt.atoms.device>`).

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.selfenergy.save.mean

      fdfparam:TBT!SelfEnergy.Save.Mean

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.SelfEnergy.Save.Mean

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      If **true** the down-folded self-energies will be
      :math:`k`-averaged after TBtrans has finished.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.selfenergy.only

      fdfparam:TBT!SelfEnergy.Only

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.SelfEnergy.Only

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      If **true** this will *only* calculate and store the down-folded
      self-energies. No physical quantities will be calculated and
      TBtrans will quit.

.. container:: fdfentrycontainer fdfentry-string

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.selfenergy.precision

      fdfparam:TBT.CDF!SelfEnergy.Precision

   .. container:: fdfparamtype

      string

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.SelfEnergy.Precision

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Precision<fdfparam:tbt.cdf.precision>`>

   .. container:: fdfentrycontainerbody

      Specify the precision used for storing the self-energies in
      NetCDF4.

.. container:: fdfentrycontainer fdfentry-integer

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.selfenergy.compress

      fdfparam:TBT.CDF!SelfEnergy.Compress

   .. container:: fdfparamtype

      integer

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.SelfEnergy.Compress

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      <<:ref:`TBT.CDF.Compress<fdfparam:tbt.cdf.compress>`>>

      Specify the compression of the self-energies in NetCDF4.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.selfenergy.mpi

      fdfparam:TBT.CDF!SelfEnergy.MPI

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.SelfEnergy.MPI

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      If **true** TBtrans will use MPI when writing the NetCDF files
      containing the downfolded self-energies.

Projected transmissions
~~~~~~~~~~~~~~~~~~~~~~~

The transmission through a scattering region is determined by the
electrodes band-structure and the energy levels for the scattering part.
In for instance molecular electronics it is often useful to determine
which molecular orbitals are responsible for the transmission as well as
knowing their hybridization with the substrate (electrodes).

TBtrans implements an advanced projection method which splits the
transmission and DOS into eigenstate projectors.

In the following we concentrate on a :math:`2` terminal device while it
may be used for :math:`N` electrode calculations. One important aspect
of projection is that the self-energies that are to be projected *must*
be fully located on the projection region. TBtrans will die if this is
not enforced. A projection can *only* be performed if the down-folding
of the self-energies for the projected electrode is fully encapsulated
in the device region
(:ref:`TBT.Atoms.Device<fdfparam:tbt.atoms.device>`).
I.e. one should reduce the device region such that any couplings from
the electrodes only couple into the projection region. Generally for the
most simple projections the device region should be equivalent to the
projection region in case there is only one projection region.

These projections should not be confused with local DOS which may be
obtained if compiled with the NetCDF4 library and via the use of sisl,
see
Sec. :ref:`physical<sec:physical>`.

**NOTE:** if the
:ref:`TBT.Projs<fdfparam:tbt.projs>`
block is defined, then the
:ref:`TBT.Projs.T<fdfparam:tbt.projs.t>`
block is required in the input unless
:ref:`TBT.Projs.Init<fdfparam:tbt.projs.init>`
is **true**.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs

      fdfparam:TBT.Projs

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      List of molecular projections used:

      ::

           %block TBT.Projs
             M-L
             M-R
           %endblock

      This tells TBtrans that two projections will exist. Each
      projection setup will be read in
      :ref:`TBT.Proj.\<\><fdfparam:tbt.proj.\<\>>`.

      There is no limit to the number of projection molecules.

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.proj.<>

      fdfparam:TBT.Proj!<>

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Proj.<>

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      Block that designates a molecular projection by the names
      specified in the
      :ref:`TBT.Projs<fdfparam:tbt.projs>`
      block.

      This block determines how each projection is interpreted, it
      consists of several options defined below:

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            atom

         .. container:: labelcontainer
            :name: fdfparam:tbt.proj.<>.atom

            fdfparam:TBT.Proj!<>!atom

         .. container:: labelcontainer
            :name: fdfparam:tbt.proj.<>.position

            fdfparam:TBT.Proj!<>!position

         There may be several atom lines. The full set of atomic indices
         will be used as a sub-space for the Hamiltonian. The atoms may
         be defined via these variants

         .. container:: fdfoptionscontainer

            .. container:: optioncontainer

               atom *A* [*B* [*C* […]]]

            A sequence of atomic indices which are used for the
            projection.

            .. container:: optioncontainer

               atom from *A* to *B* [step *s*]

            Here atoms *A* up to and including *B* are used. If step <s>
            is given, the range *A*:*B* will be taken in steps of *s*.

            ::

                       atom from 3 to 10 step 2

            will add atoms 3, 5, 7 and 9.

            .. container:: optioncontainer

               atom from *A* plus/minus *B* [step *s*]

            Atoms *A* up to and including :math:`\emph{A}+\emph{B}-1`
            are added to the projection. If step <s> is given, the range
            *A*::math:`\emph{A}+\emph{B}-1` will be taken in steps of
            *s*.

            .. container:: optioncontainer

               atom [<A>, *B* -- *C* [step *s*], *D*]

            Equivalent to from …to specification, however in a shorter
            variant. Note that the list may contain arbitrary number of
            ranges and/or individual indices.

            ::

                       atom [2, 3 -- 10 step 2, 6]

            will add atoms 2, 3, 5, 7, 9 and 6.

         .. container:: optioncontainer

            Gamma

         .. container:: labelcontainer
            :name: fdfparam:tbt.proj.<>.gamma

            fdfparam:TBT.Proj!<>!Gamma

         Logical variable which determines whether the projectors are
         the :math:`\Gamma`-point projectors, or the :math:`k` resolved
         ones. For :math:`\Gamma`-only calculations this has no effect.
         If the eigenstates are non-dispersive in the Brillouin zone
         there should be no difference between **true** or **false**.

         **NOTE:** it is *very* important to know the dispersion and
         possible band-crossings of the eigenstates if this option is
         **false**. For band-crossings one must manually perform the
         projections for the :math:`k`-points in a stringent manner as
         the order of eigenstates are not retained.

         .. container:: optioncontainer

            proj <P-name>

         .. container:: labelcontainer
            :name: fdfparam:tbt.proj.<>.proj

            fdfparam:TBT.Proj!<>!proj

         Allows to define a projection based on the eigenstates for the
         current molecule.

         The <P-name> designates the name associated with this
         projection.

         It is parsed like this, in the following :math:`0` is the Fermi
         level (HOMO :math:`=-1`, LUMO :math:`=1`):

         .. container:: fdfoptionscontainer

            .. container:: optioncontainer

               level from <E1> to <E2>

            Energy eigenstates E1 and E2 will be part of the molecular
            orbitals that constitute this projection

            .. container:: optioncontainer

               level from <E> plus <N>

            Energy eigenstates between E and :math:`E+N-1` will be part
            of the molecular orbitals that constitute this projection

            .. container:: optioncontainer

               level from <E> minus <N>

            Energy eigenstates between E and :math:`E-N+1` will be part
            of the molecular orbitals that constitute this projection

            .. container:: optioncontainer

               level <E1> <E2> ... <En>

            All eigenstates specified will be part of the molecular
            orbitals that constitute this projection

            .. container:: optioncontainer

               level [ <list> ]

            A comma-separated list specification.

            .. container:: optioncontainer

               end

            All gathered eigenstates so far will constitute the
            projection named <P-name>

         Note that level :math:`0` refers to the Fermi level, it will be
         silently removed as it is not an eigenstate, so you do not need
         to think about it.

         You can specify named projection blocks as many times as you
         want.

         To conclude the full projection block here is an example
         describing three different projections for the left molecule in

         ::

             %block TBT.Proj.M-L
               # We have 2 atoms on this molecule
               atom from 5 plus 2
               # We only do a Gamma projection
               Gamma .true.
               # We will utilise three different projections on 
               # this molecule
               proj HOMO
                level -1
               end 
               proj LUMO
                level 1
               end
               proj H-plus-L
                level from -1 to 1
               end
             %endblock

         Similarly for the right molecule we do

         ::

             %block TBT.Proj.M-R
               # We have 2 atoms on this molecule
               atom from 8 plus 2
               # We only do a Gamma projection
               Gamma .true.
               # We will utilise three different projections on 
               # this molecule
               proj HOMO
                level -1
               end 
               proj LUMO
                level 1
               end
               proj H-plus-L
                level from -1 to 1
               end
             %endblock

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.proj.<>.states

      fdfparam:TBT.Proj!<>!States

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Proj.<>.States

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Save all states for the projection. The saved quantity can be
      post-processed to decipher the locality of each projection.

      In the NetCDF file there will be two variables: ``state`` and
      ``states`` where the former will contain
      :math:`\textbf{S}^{1/2}|i\rangle`, while the latter will contain
      :math:`|i\rangle`.

      *Needed if you wish to select specific molecular orbitals
      dependent on the nature of the molecular orbital.*

.. container:: fdfentrycontainer fdfentry-integer

   .. container:: labelcontainer
      :name: fdfparam:tbt.cdf.proj.compress

      fdfparam:TBT.CDF!Proj.Compress

   .. container:: fdfparamtype

      integer

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.CDF.Proj.Compress

      .. container:: fdfparamdefault

         <:ref:`TBT.CDF.Compress<fdfparam:tbt.cdf.compress>`>

   .. container:: fdfentrycontainerbody

      Allows a different compression for the projection file. The
      projection file is typically larger than the default output file,
      so compression of them separately might be needed.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.init

      fdfparam:TBT.Projs!Init

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.Init

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Whether TBtrans will only create the projection tables and then
      quit.

      As TBtrans allows to re-use the projection file the user can
      choose to stop right after creation. Specifically it will allow
      one to swap projection states with other projection states. This
      can be useful when bias is applied and the hybridisation
      “destroys” the molecule Hamiltonian. After initialising the
      projection tables the user can manually swap them with those
      calculated at zero bias, thus retaining the same projection tables
      for different bias’.

      Note that for spin calculations you need to utilise the
      :ref:`TBT.Spin<fdfparam:tbt.spin>`
      flag to initialise both projection files (spin UP *and* spin DOWN)
      before proceeding with the calculation.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.debug

      fdfparam:TBT.Projs!Debug

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.Debug

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Print out additional information regarding the projections. It
      will print out assertion lines orthogonality.

      *Possibly not useful for other than the developers.*

.. container:: fdfentrycontainer fdfentry-block

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.t

      fdfparam:TBT.Projs!T

   .. container:: fdfparamtype

      block

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.T

      .. container:: fdfparamdefault

         <none>

   .. container:: fdfentrycontainerbody

      As one might specify *many* molecular projections to investigate a
      lot of details of the system it seems perilous to always calculate
      all allowed transmission permutations.

      Instead the user has to supply the permutations of transport that
      is calculated. This block will let the user decide which to
      calculate and which to not.

      In the following Left(L)/Right(R) corresponds to
      :math:`T=\mathop{\mathrm{Tr}}[\textbf{G}\boldsymbol\Gamma_L\textbf{G}^\dagger\boldsymbol\Gamma_R]`
      where Left, Right are found in the
      :ref:`TBT.Elecs<fdfparam:tbt.elecs>`
      block.

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            from <proj-L> to

         Projects :math:`\boldsymbol\Gamma_L` on to the <projection>
         before doing the R projections.

         The R projections are constructed in the following lines until
         end is seen.

         .. container:: fdfoptionscontainer

            .. container:: optioncontainer

               <proj-R>

            Projects :math:`\boldsymbol\Gamma_R` on to the <projection>
            which then calculates the transmission

      Each projection can be represented in three different ways:

      .. container:: fdfoptionscontainer

         .. container:: optioncontainer

            <elec>

         Makes no projection on the scattering matrix

         .. container:: optioncontainer

            <elec>.<name>

         Makes all permutations of the projections attached to the
         molecule named <name>

         .. container:: optioncontainer

            <elec>.<name>.<P-name>

         Projects the named projection <P-name> from molecule <name>
         onto electrode <elec>

      An example input for projection two molecules could be:

      ::

          %block TBT.Projs.T
            from Left.M-L.HOMO to
              Right.M-R
              Right
            end
            from Left.M-L.LUMO to
              Right.M-R.LUMO
            end
          %endblock

      which will be equivalent to the more verbose

      ::

          %block TBT.Projs.T
            from Left.M-L.HOMO to
              Right.M-R.HOMO
              Right.M-R.LUMO
              Right.M-R.H-plus-L
              Right
            end
            from Left.M-L.LUMO to
              Right.M-R.LUMO
            end
          %endblock

      This will calculate the transport using all these equations

      .. math::

         \begin{aligned}
           
             

           T_{|H_1\rangle,|H_2\rangle} &=\mathop{\mathrm{Tr}}\big[\textbf{G}|H_1\rangle\langle H_1|\boldsymbol\Gamma_L|H_1\rangle\langle H_1|\textbf{G}^\dagger|H_2\rangle\langle H_2|\boldsymbol\Gamma_R|H_2\rangle\langle H_2|\big]
           \\
           
             

           T_{|H_1\rangle,|L_2\rangle} &=\mathop{\mathrm{Tr}}\big[\textbf{G}|H_1\rangle\langle H_1|\boldsymbol\Gamma_L|H_1\rangle\langle H_1|\textbf{G}^\dagger|L_2\rangle\langle L_2|\boldsymbol\Gamma_R|L_2\rangle\langle L_2|\big]
           \\
           
             

           T_{|H_1\rangle,|H_2\rangle+|L_2\rangle}
           &=\mathop{\mathrm{Tr}}\Big[\textbf{G}|H_1\rangle\langle H_1|\boldsymbol\Gamma_L|H_1\rangle\langle H_1|\textbf{G}^\dagger\big(|H_2\rangle\langle H_2|+|L_2\rangle\langle L_2|\big)\boldsymbol\Gamma_R\big(|H_2\rangle\langle H_2|+|L_2\rangle\langle L_2|\big)\Big]
           \\
           
             

           T_{|H_1\rangle,R} &=\mathop{\mathrm{Tr}}\Big[\textbf{G}|H_1\rangle\langle H_1|\boldsymbol\Gamma_L|H_1\rangle\langle H_1|\textbf{G}^\dagger\boldsymbol\Gamma_R\Big]
           \\
           
             

           T_{|L_1\rangle,|L_2\rangle} &=\mathop{\mathrm{Tr}}\big[\textbf{G}|L_1\rangle\langle L_1|\boldsymbol\Gamma_L|L_1\rangle\langle L_1|\textbf{G}^\dagger|L_2\rangle\langle L_2|\boldsymbol\Gamma_R|L_2\rangle\langle L_2|\big]
         \end{aligned}

      Notice that Eq. `[eq:T:two:one] <#eq:T:two:one>`__ is equivalent
      to `[eq:T:single:one] <#eq:T:single:one>`__ in our two state
      model.

      Note that removing an explicit named projection allows easy
      creation of all available permutations of the projection states
      associated with the molecule.

      By default some electrodes are not accessible for projections
      unless
      :ref:`TBT.Projs.T.Out<fdfparam:tbt.projs.t.out>`
      or
      :ref:`TBT.Projs.T.All<fdfparam:tbt.projs.t.all>`
      are **true**.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.only

      fdfparam:TBT.Projs!Only

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.Only

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Whether TBtrans will not calculate non-projected transmissions. If
      you are only interested in the projection transmissions and/or
      have already calculated the non-projected transmissions you can
      use this option.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.dos.a

      fdfparam:TBT.Projs!DOS.A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.DOS.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Save the spectral density of states for the projections. In case
      you have set
      :ref:`TBT.DOS.A<fdfparam:tbt.dos.a>`
      this will default to that flag.

      In case any of
      :ref:`TBT.Projs.Current.Orb<fdfparam:tbt.projs.current.orb>`,
      :ref:`TBT.Projs.DM.A<fdfparam:tbt.projs.dm.a>`,
      :ref:`TBT.Projs.COOP.A<fdfparam:tbt.projs.coop.a>`
      or
      :ref:`TBT.Projs.COHP.A<fdfparam:tbt.projs.cohp.a>`
      is **true** this flag will be set to **true** as well.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.current.orb

      fdfparam:TBT.Projs!Current.Orb

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.Current.Orb

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Will calculate and save the orbital current for the device with
      the projections.

      The orbital current will be saved in the same sparsity pattern as
      the cut-out device region sparsity pattern.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.dm.a

      fdfparam:TBT.Projs!DM.A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.DM.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate the energy and :math:`k`-resolved density matrix for the
      projected spectral functions. The density matrix may be used to
      construct real-space LDOS profiles.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.coop.a

      fdfparam:TBT.Projs!COOP.A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.COOP.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate COOP from the projected spectral function in the device
      region.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.cohp.a

      fdfparam:TBT.Projs!COHP.A

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.COHP.A

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Calculate COHP from the projected spectral function in the device
      region.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.t.all

      fdfparam:TBT.Projs!T.All

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.T.All

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Same as
      :ref:`TBT.T.All<fdfparam:tbt.t.all>`,
      but for projections. If differing projections are performed on the
      scattering states the transmission will not be reversible. You can
      turn on all projection operations using this flag.

.. container:: fdfentrycontainer fdfentry-logical

   .. container:: labelcontainer
      :name: fdfparam:tbt.projs.t.out

      fdfparam:TBT.Projs!T.Out

   .. container:: fdfparamtype

      logical

   .. container:: fdfentryheader

      .. container:: fdfparamname

         TBT.Projs.T.Out

      .. container:: fdfparamdefault

         false

   .. container:: fdfentrycontainerbody

      Same as
      :ref:`TBT.T.Out<fdfparam:tbt.t.out>`
      for projections.

NetCDF4 support
~~~~~~~~~~~~~~~

TBtrans stores all relevant physical quantities in the TBT.nc file which
retains orbital resolved DOS, orbital currents, transmissions,
transmission eigenvalues, etc. One may use sisl to easily analyze and
extract quantities from this file using Python.

These files are created if NetCDF4 support is enabled

.. container:: description

   File which contain nearly everything calculated in TBtrans. The
   structure of this file is a natural tree structure to accommodate
   :math:`N` electrode output.

   see
   :ref:`TBT.SelfEnergy.Save<fdfparam:tbt.selfenergy.save>`

   Down-folded self-energies are stored in this file.

   see
   :ref:`TBT.Projs<fdfparam:tbt.projs>`

   Stores projected DOS, transmission and/or orbital currents. Using
   projections for large :math:`k` and energy sampling will create very
   large files. Ensure that you have a large amount of disk-space
   available.

   see
   :ref:`TBT.DOS.Gf<fdfparam:tbt.dos.gf>`

   The :math:`k` resolved density of states from the Green function.

   see
   :ref:`TBT.DOS.Gf<fdfparam:tbt.dos.gf>`

   The :math:`k` averaged density of states from the Green function.

   see
   :ref:`TBT.DOS.A<fdfparam:tbt.dos.a>`

   The :math:`k` resolved density of states from the electrode name <>.

   see
   :ref:`TBT.DOS.A<fdfparam:tbt.dos.a>`

   The :math:`k` averaged density of states from the electrode name <>.

   The :math:`k` resolved transmission from <1> to <2>.

   The :math:`k` averaged transmission from <1> to <2>.

   see
   :ref:`TBT.T.Out<fdfparam:tbt.t.out>`

   The :math:`k` resolved correction to the transmission for <1>.

   see
   :ref:`TBT.T.Out<fdfparam:tbt.t.out>`

   The :math:`k` averaged correction to the transmission for <1>.

   see
   :ref:`TBT.T.Eig<fdfparam:tbt.t.eig>`

   The :math:`k` resolved transmission eigenvalues from <1> to <2>.

   see
   :ref:`TBT.T.Eig<fdfparam:tbt.t.eig>`

   The :math:`k` averaged transmission eigenvalues from <1> to <2>.

   see
   :ref:`TBT.T.Out<fdfparam:tbt.t.out>`

   The :math:`k` resolved correction eigenvalues for <1>.

   see
   :ref:`TBT.T.Out<fdfparam:tbt.t.out>`

   The :math:`k` averaged correction eigenvalues for <1>.

   see
   :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`/:ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`

   The :math:`k` resolved bulk density of states of electrode <>.

   see
   :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`/:ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`

   The :math:`k` averaged bulk density of states of electrode <>.

   see
   :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`/:ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`

   The :math:`k` resolved bulk transmission of electrode <>.

   see
   :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`/:ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`

   The :math:`k` averaged bulk transmission of electrode <>.

All the above files will only be created if TBtrans was successfully
executed and their respective options was enabled.

No NetCDF4 support
~~~~~~~~~~~~~~~~~~

In case TBtrans is not compiled with NetCDF4 support TBtrans is heavily
limited in functionality and subsequent analysis. Particularly the DOS
quantities are not orbital resolved. Also none of the quantities will be
:math:`k` averaged, this is required to be done externally.

The following files are created:

.. container:: description

   see
   :ref:`TBT.DOS.Gf<fdfparam:tbt.dos.gf>`

   The :math:`k` resolved density of states from the Green function.

   see
   :ref:`TBT.DOS.A<fdfparam:tbt.dos.a>`

   The :math:`k` resolved density of states from the electrode name <>.

   The :math:`k` resolved transmission from <1> to <2>.

   see
   :ref:`TBT.T.Eig<fdfparam:tbt.t.eig>`

   The :math:`k` resolved transmission eigenvalues from <1> to <2>.

   see
   :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`/:ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`

   The :math:`k` resolved bulk density of states of electrode <>.

   see
   :ref:`TBT.DOS.Elecs<fdfparam:tbt.dos.elecs>`/:ref:`TBT.T.Bulk<fdfparam:tbt.t.bulk>`

   The :math:`k` resolved bulk transmission of electrode <>.

.. container:: references csl-bib-body hanging-indent
   :name: refs

   .. container:: csl-entry
      :name: ref-sisl

      Papior, Nick R. 2020. “Sisl.”
      https://doi.org/10.5281/zenodo.597181.

   .. container:: csl-entry
      :name: ref-Papior2019

      Papior, Nick, Gaetano Calogero, Susanne Leitherer, and Mads
      Brandbyge. 2019. “Removing all periodic boundary conditions:
      Efficient nonequilibrium Green’s function calculations.” *Physical
      Review B* 100 (19): 195417.
      https://doi.org/10.1103/PhysRevB.100.195417.

   .. container:: csl-entry
      :name: ref-Papior2017

      Papior, Nick, Nicolás Lorente, Thomas Frederiksen, Alberto García,
      and Mads Brandbyge. 2017. “Improvements on non-equilibrium and
      transport Green function techniques: The next-generation
      TranSiesta.” *Computer Physics Communications* 212 (March): 8–24.
      https://doi.org/10.1016/j.cpc.2016.09.022.

.. [1]
   In :math:`N>2`-electrode calculations one *cannot* use this quantity
   to calculate the total current out of an electrode.

.. [2]
   Much like BandLines in SIESTA.

.. [3]
   `www.graphviz.org <www.graphviz.org>`__
