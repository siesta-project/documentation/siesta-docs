grep enth $1 | awk '{print NR, $4}' >enth.data

gnuplot -persist <<-EOFMarker
set term png
set output "energy_iteration.png"
set xlabel '# iteration'
set ylabel 'Energy (eV)'
p 'enth.data' u 1:2 w l


EOFMarker
