# Disclaimer: This is a simple code written for plotting 
# the phonon bands of silicon for the SIESTA school.
# Please adapt it to suit your specific needs. RF


set terminal postscript eps size 8,6 color enhanced font "Helvetica, 25"
set output "bands.eps"

set xlabel "K-points" font "Helvetica, 25"
set ylabel "Frequency (cm-1)" font "Helvetica, 25"

#defining the positions of k-points (according with the input file) 
#and the vertical lines in correspondance to the high-symmetry points

set xtics font "Helvetica, 25" ( "{/Symbol G}" 0, "X" 0.6, "{/Symbol G}" 1.45, "L" 1.97 )
set ytics font "Helvetica, 25"
set arrow from 1.45, graph 0 to 1.45, graph 1 nohead lc rgb "black" lw 1
set arrow from 1.97, graph 0 to 1.97, graph 1 nohead lc rgb "black" lw 1                           
set arrow from 0.6, graph 0 to 0.6, graph 1 nohead lc rgb "black" lw 1
set xrange [0:2]

plot "Si.phonon-bands.111.dat" using 1:2 with lines lw 3 t "111", "Si.phonon-bands.222.dat" u 1:2 with lines lw 3 t "222", "Si.phonon-bands.333.dat" u 1:2 w l lw 3 t "333"

set output
